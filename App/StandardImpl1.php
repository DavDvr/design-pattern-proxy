<?php

namespace Proxy\App;

/**
 * Class StandardImpl1 implémente l'interface
 * Cette class définit l'objet que l'objet Proxy représente
 * @package Proxy\App
 */
class StandardImpl1 implements Standard
{

    public function process(): void
    {
        echo "Process.....StandardImpl1<br>";
    }
}