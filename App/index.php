<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home Proxy</title>
</head>
<body>

<?php

use Proxy\App\Client;
use Proxy\App\Proxy;
require_once (dirname(__DIR__)."/vendor/autoload.php");

//création ddu client
$client = new Client();

//utilisation du proxy
$client->setStandard(new Proxy());

//appel à la methode process du proxy
$client->process();

?>

</body>
</html>
