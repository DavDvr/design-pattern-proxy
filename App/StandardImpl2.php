<?php

namespace Proxy\App;

/**
 * Class StandardImpl2 implémente l'interface
 * Cette class définit l'objet que l'objet Proxy représente
 * Class StandardImpl2
 * @package Proxy\App
 */
class StandardImpl2 implements Standard
{

    public function process(): void
    {
        echo "Process.....StandardImpl2<br>";
    }
}