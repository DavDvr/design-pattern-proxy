<?php

namespace Proxy\App;

/**
 * Class Client fait un appel dans sa methode process() à la methode process de l'interface Standard
 * @package Proxy\App
 */
class Client
{
    private Standard $standard;

    public function process()
    {
        $this->standard->process();
    }

    /**
     * @return Standard
     */
    public function getStandard(): Standard
    {
        return $this->standard;
    }

    /**
     * @param Standard $standard peut être le proxy
     */
    public function setStandard(Standard $standard): void
    {
        $this->standard = $standard;
    }

}