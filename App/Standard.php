<?php

namespace Proxy\App;

/**
 * Interface Standard : definit l'interface des classes StandardImpl1, StandardImpl2  et Proxy
 * @package Proxy\App
 */
interface Standard
{
    public function process(): void;
}