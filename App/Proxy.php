<?php

namespace Proxy\App;
use Exception;
use RuntimeException;

/**
 * Class Proxy fournit un intermédiaire entre la partie cliente et l'objet StandardImpl1, StandardImpl2
 * Cet intermédiaire peut avoir plusieurs buts (synchronisation, contrôle d'accès, cache, accès distant,..)
 *
 * @package Proxy\App
 */
class Proxy implements Standard
{
    private StandardImpl1 $target;

    /**
     * La methode process de proxy va réaliser des verifications de sécurités par exemple
     * Contrôle de l'accès à un objet distant, recupération des informations en caches..
     * Cela ne sert a rien de charger la mémoire de façon inutile.
     * C'est seulement si les conditions de sécurité sont ok que j'instancie mon objet
     * @throws Exception
     */
    public function process(): void
    {
        echo "Vérification du context de sécurité<br>";

        //génère true ou false de façon aléatoire
        $b = (bool)random_int(0, 1);
        if ($b){
            echo "Avant l'appel<br>";
            $this->target = new StandardImpl1();
            $this->target->process();
            echo "Après l'appel d'autre opérations, exemple: mis en cache , fermeture de la connexion, transformer le resultat<br>";
        }else{
            throw new RuntimeException("Forbidden 403!");
        }
    }
}